# dysqimod

Classifier model package based on signal quality indices of swallowing sEMG signals to reject poor quality signals.